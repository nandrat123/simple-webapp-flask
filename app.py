import os
from flask import Flask
app = Flask(__name__)

@app.route("/")
def main():
    return "Welcomee!"

@app.route('/how to adapt agile')
def hello():
    return 'If developer can not change,then change the developer. Regards, KM'

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8080)
